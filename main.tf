terraform {
    required_providers {
        aws = {
        source  = "hashicorp/aws"
        version = "~> 4.0"
        }
    }
    backend "http" {
    }
}

provider "aws" {
  region = "us-east-2"
}

resource "aws_s3_bucket" "staging" {

  tags = {
    Environment = "Dev"
  }
}

resource "aws_s3_object" "object" {
  bucket      = aws_s3_bucket.staging.bucket
  key         = "g-hello-0.0.1-SNAPSHOT.jar"
  source      = "build/g-hello-0.0.1-SNAPSHOT.jar"
  source_hash = filemd5("build/g-hello-0.0.1-SNAPSHOT.jar")
}


+




#   resource "aws_s3_bucket_policy" "allow_access_from_another_account" {
#     bucket = karen-tf-test-bucket
#     policy = data.aws_iam_policy_document.allow_access_from_another_account.json
#   }


#   resource "aws_s3_bucket_acl" "example" {
#     bucket = aws_s3_bucket.example.id
#     acl    = "private"
#   }

#   data "aws_iam_policy_document" "allow_access_from_another_account" {
#     statement {
#       principals {
#         type        = "AWS"
#         identifiers = ["*"]
#       }

#       actions = [
#         "s3:GetObject",
#         "s3:ListBucket",
#       ]

#       resources = [
#         aws_s3_bucket.example.arn,
#         "${aws_s3_bucket.example.arn}/*",
#       ]
#     }
#   }






# data "aws_iam_policy_document" "assume_role" {
#   statement {
#     effect = "Allow"

#     principals {
#       type        = "Service"
#       identifiers = ["ec2.amazonaws.com"]
#     }

#     actions = ["sts:AssumeRole"]
#   }
# }

# resource "aws_iam_role" "role" {
#   name               = "krayon_test_role"
#   path               = "/"
#   assume_role_policy = data.aws_iam_policy_document.assume_role.json
# }

# data "aws_ami" "ubuntu" {
#   most_recent = true

#   filter {
#     name   = "name"
#     values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
#   }

#   filter {
#     name   = "virtualization-type"
#     values = ["hvm"]
#   }

#   owners = ["099720109477"] # Canonical
# }

# resource "aws_instance" "web" {
#   ami           = data.aws_ami.ubuntu.id
#   instance_type = "t3.micro"

#   tags = {
#     Name = "HelloWorld"
#   }
